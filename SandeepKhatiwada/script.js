const form = document.getElementById('form');
const username = document.getElementById('username');
const address = document.getElementById('address');
const telephone = document.getElementById('telephone');
const DOB = document.getElementById('DOB');
const email = document.getElementById('email');
const password = document.getElementById("password");
const confirmPassword = document.getElementById("confirmPassword");
const button = document.getElementById("submit");

//creating alert for mismatched passwords
var inspect = 1;
function check(elem) {
    if (elem.value.length > 0) {
        if (elem.value != password.value){
            document.getElementById('alert').innerText = "Passwords do not match.";
            inspect = 0;
        }else{
            document.getElementById('alert').innerText = "";
            inspect = 1;
        }}else{
            document.getElementById('alert').innerText = "Please confirm the password.";
            inspect = 0;
        }
    }

//setting error and success alerts    
const setError = (element, message) => {
    const inputControl = element.parentElement;
    const errorDisplay = inputControl.querySelector('.error');

    errorDisplay.innerText = message;
    inputControl.classList.add('error');
    inputControl.classList.remove('success')
}

const setSuccess = element => {
    const inputControl = element.parentElement;
    const errorDisplay = inputControl.querySelector('.error');

    errorDisplay.innerText = '';
    inputControl.classList.add('success');
    inputControl.classList.remove('error');
};

//validating the userinputs with our requirements
const validateInputs = () => {
    const telephoneValue = telephone.value.trim();
    const DOBValue = DOB.value.trim();
    const emailValue = email.value.trim();
    const passwordValue = password.value.trim()
    
    //calculating age
    var today = new Date();
    var date = today.getFullYear()

    var d = DOBValue.substring(0,4);
    age = date-d
        if (age<'13'){
            alert("Warning: Invalid Age");
        } else if (age>'50'){
            alert("Warning: Age above 50. Please access the help link.")
        }

    if(telephoneValue === '') {
        setError(telephone, 'Phone number is required with country code (+61)');
    }  
     else if (telephoneValue.match (/^\+?([0-9]{3})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/)){
        setSuccess(telephone);
    } else {
        setError(telephone, 'Phone number is required with country code (+61)')
    }

    if(DOBValue === '') {
        setError(DOB, 'Date of birth is required');
    } else if (age < '13') {
        setError(DOB, 'You must be above 13 to sign up.')
    } 
    else {
        setSuccess(DOB);
    }

    if(emailValue === '') {
        setError(email, 'Email is required');
    } else if (!isValidEmail(emailValue)) {
        setError(email, 'Provide a valid email address');
    } else {
        setSuccess(email);
    }

    if (passwordValue.length < '8' || passwordValue.length > '20') {
        setError(password, 'Password must be between 8 and 20 characters.');
    } else {
        setSuccess(password);
    }

}

const isValidEmail = email => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

form.addEventListener('submit', e => {
    validateInputs();
    e.preventDefault();
    // form.reset()
})